package misEileToimus.miseiletoimusapi;

import misEileToimus.miseiletoimusapi.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HomepageController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/greeting")
    public String getGreeting() {
        return "Tere!!!";

    }

    @GetMapping("/quotes")
    public List<Quote> getQoutes() {

        List<Quote> quotes = jdbcTemplate.query("select * from quotes order by rand(dayofyear(CURRENT_DATE)) limit 1 ",
                (row, count) -> {
                    int qouteId = row.getInt("qId");
                    String qouteBody = row.getString("qBody");
                    String qouteAuthor = row.getString("qAuthor");


                    Quote quote = new Quote();
                    quote.setId(qouteId);
                    quote.setBody(qouteBody);
                    quote.setAuthor(qouteAuthor);


                    return quote;

                }
        );
        return quotes;

    }

    @GetMapping("/videos")
    public List<Video> getVideos() {

        List<Video> videos = jdbcTemplate.query("select * from videos order by rand(dayofyear(CURRENT_DATE)) limit 1 ",
                (row, count) -> {
                    int videoId = row.getInt("vId");
                    String videoUrl = row.getString("vUrl");

                    Video video = new Video();
                    video.setId(videoId);
                    video.setUrl(videoUrl);

                    return video;

                }
        );
        return videos;

    }

    @GetMapping("/lifehacks")
    public List<Lifehack> getLifehack() {

        List<Lifehack> lifehacks = jdbcTemplate.query("select * from lifehack order by rand(dayofyear(CURRENT_DATE)) limit 1 ",
                (row, count) -> {
                    int lifehackId = row.getInt("hackId");
                    String lifehackUrl = row.getString("hUrl");

                    Lifehack lifehack = new Lifehack();
                    lifehack.setId(lifehackId);
                    lifehack.setUrl(lifehackUrl);

                    return lifehack;

                }
        );
        return lifehacks;

    }

    @GetMapping("/uselesss")
    public List<Useless> getUseless() {

        List<Useless> uselesss = jdbcTemplate.query("select * from useless order by rand()",
                (row, count) -> {
                    int uselessId = row.getInt("uId");
                    String uselessUrl = row.getString("uUrl");

                    Useless useless = new Useless();
                    useless.setId(uselessId);
                    useless.setUrl(uselessUrl);

                    return useless;

                }
        );
        return uselesss;

    }




    @PostMapping("/useless")
    public void addUseless(@RequestBody Useless useless) {
        jdbcTemplate.update("insert into useless (uUrl) values (?)", useless.getUrl());

    }

    @PostMapping("/uselesss")
    public void addUseless(@RequestBody Useless[] uselesss) {

        for (Useless useless : uselesss) {
            addUseless(useless);
        }


    }








}


