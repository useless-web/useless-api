package misEileToimus.miseiletoimusapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@SpringBootApplication
public class MiseiletoimusApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiseiletoimusApiApplication.class, args);



}
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}



