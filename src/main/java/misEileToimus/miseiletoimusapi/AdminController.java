package misEileToimus.miseiletoimusapi;

import misEileToimus.miseiletoimusapi.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/videos")
    public List<Video> getVideos() {

        List<Video> videos = jdbcTemplate.query("select * from videos ",
                (row, count) -> {
                    int videoId = row.getInt("vId");
                    String videoUrl = row.getString("vUrl");

                    Video video = new Video();
                    video.setId(videoId);
                    video.setUrl(videoUrl);

                    return video;

                }
        );
        return videos;

    }
    @GetMapping("/video/{id}")
    public Video getVideo(@PathVariable int id) {

        Video result = jdbcTemplate.queryForObject("select * from videos where vId = ? ", new Object[]{id}, (row, count) -> {
            int videoId = row.getInt("vId");
            String videoUrl = row.getString("vUrl");

            Video video = new Video();
            video.setId(videoId);
            video.setUrl(videoUrl);


            return video;
        });

        return result;
    }

    @PostMapping("/video")
    public void addVideo(@RequestBody Video video) {
        jdbcTemplate.update("insert into videos (vUrl) values (?)", video.getUrl());

    }

    @PostMapping("/videos")
    public void addVideos(@RequestBody Video[] videos) {

        for (Video video : videos) {
            addVideo(video);
        }


    }
    @PutMapping("/video")
    public void editVideo (@RequestBody Video video) {
        jdbcTemplate.update("update videos set vUrl = ? where vId = ?",
                video.getUrl(), video.getId());
    }

    @DeleteMapping("/video/{id}")
    public void deleteVideo(@PathVariable int id) {

        jdbcTemplate.update("delete from videos where vId = ?", id);

    }

    @GetMapping("/quotes")
    public List<Quote> getQoutes() {

        List<Quote> quotes = jdbcTemplate.query("select * from quotes ",
                (row, count) -> {
                    int qouteId = row.getInt("qId");
                    String qouteBody = row.getString("qBody");
                    String qouteAuthor = row.getString("qAuthor");


                    Quote quote = new Quote();
                    quote.setId(qouteId);
                    quote.setBody(qouteBody);
                    quote.setAuthor(qouteAuthor);


                    return quote;

                }
        );
        return quotes;

    }
    @GetMapping("/quote/{id}")
    public Quote getQuote(@PathVariable int id) {

        Quote result = jdbcTemplate.queryForObject("select * from quotes where qId = ? ", new Object[]{id}, (row, count) -> {
            int quoteId = row.getInt("qId");
            String quoteBody = row.getString("qBody");
            String quoteAutohr = row.getString("qAuthor");

            Quote quote = new Quote();
            quote.setId(quoteId);
            quote.setBody(quoteBody);
            quote.setAuthor(quoteAutohr);

            return quote;
        });

        return result;


    }



    @PostMapping("/quote")
    public void addQuote(@RequestBody Quote quote) {
        jdbcTemplate.update("insert into quotes (qBody,qAuthor) values (?,?)", quote.getBody(), quote.getAuthor());

    }

    @PostMapping("/quotes")
    public void addQuote(@RequestBody Quote[] quotes) {

        for (Quote quote : quotes) {
            addQuote(quote);
        }


    }

    @DeleteMapping("/quote/{id}")
    public void deleteQuote(@PathVariable int id) {

        jdbcTemplate.update("delete from quotes where qId = ?", id);

    }
    @PutMapping("/quote")
    public void editQuote (@RequestBody Quote quote){
        jdbcTemplate.update("update quotes set qBody = ?, qAuthor = ? where qId = ?",
                quote.getBody(), quote.getAuthor(), quote.getId());

    }

    @GetMapping("/lifehacks")
    public List<Lifehack> getLifehacks() {

        List<Lifehack> lifehacks = jdbcTemplate.query("select * from lifehack",
                (row, count) -> {
                    int lifehackId = row.getInt("hackId");
                    String lifehackUrl = row.getString("hUrl");

                    Lifehack lifehack = new Lifehack();
                    lifehack.setId(lifehackId);
                    lifehack.setUrl(lifehackUrl);

                    return lifehack;

                }
        );
        return lifehacks;

    }

    @GetMapping("/lifehack/{id}")
    public Lifehack getLifehack(@PathVariable int id) {

        Lifehack result = jdbcTemplate.queryForObject("select * from lifehack where hackId = ? ", new Object[]{id}, (row, count) -> {
            int lifehackId = row.getInt("hackId");
            String lifehackUrl = row.getString("hUrl");

            Lifehack lifehack = new Lifehack();
            lifehack.setId(lifehackId);
            lifehack.setUrl(lifehackUrl);


            return lifehack;
        });

        return result;
    }
        @PostMapping("/lifehack")
        public void addLifehack (@RequestBody Lifehack lifehack){
            jdbcTemplate.update("insert into lifehack (hUrl) values (?)", lifehack.getUrl());

        }

        @PostMapping("/lifehacks")
        public void addLifehack (@RequestBody Lifehack[]lifehacks){

            for (Lifehack lifehack : lifehacks) {
                addLifehack(lifehack);
            }


        }
        @DeleteMapping("/lifehack/{id}")
        public void deleteLifehack ( @PathVariable int id){

            jdbcTemplate.update("delete from lifehack where hackId = ?", id);

        }
        @PutMapping("/lifehack")
        public void editLifehack (@RequestBody Lifehack lifehack){
            jdbcTemplate.update("update lifehack set hUrl = ? where hackId = ?",
                    lifehack.getUrl(), lifehack.getId());

        }

        @GetMapping("/uselesss")
        public List<Useless> getUseless () {

            List<Useless> uselesss = jdbcTemplate.query("select * from useless",
                    (row, count) -> {
                        int uselessId = row.getInt("uId");
                        String uselessUrl = row.getString("uUrl");

                        Useless useless = new Useless();
                        useless.setId(uselessId);
                        useless.setUrl(uselessUrl);

                        return useless;

                    }
            );
            return uselesss;

        }
    @GetMapping("/useless/{id}")
    public Useless getUseless(@PathVariable int id) {

        Useless result = jdbcTemplate.queryForObject("select * from useless where uId = ? ", new Object[]{id}, (row, count) -> {
            int uselessId = row.getInt("uId");
            String uselessUrl = row.getString("uUrl");

            Useless useless = new Useless();
            useless.setId(uselessId);
            useless.setUrl(uselessUrl);


            return useless;
        });

        return result;


    }

    @PostMapping("/useless")
        public void addUseless (@RequestBody Useless useless){
            jdbcTemplate.update("insert into useless (uUrl) values (?)", useless.getUrl());

        }
    @PutMapping("/useless")
    public void editUseless (@RequestBody Useless useless){
        jdbcTemplate.update("update useless set uUrl = ? where uId = ?",
                useless.getUrl(), useless.getId());

    }

        @PostMapping("/uselesss")
        public void addUseless (@RequestBody Useless[]uselesss){

            for (Useless useless : uselesss) {
                addUseless(useless);
            }

        }
        @DeleteMapping("/useless/{id}")
        public void deleteUseless ( @PathVariable int id){

            jdbcTemplate.update("delete from useless where uId = ?", id);

        }

    }

